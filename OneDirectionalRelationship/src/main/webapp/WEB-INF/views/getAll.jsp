<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@page import="java.util.List"%>
<%@page import="com.sachin.OneDirectionalRelationship.entity.Student"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
   
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
	function sSave() {
		document.forms[0].action="/student/sSave";
		document.forms[0].submit();
	}
	
	function sUpdate() {
		document.forms[0].action="/student/sUpdate";
		document.forms[0].submit();
	}
	
	function sGet() {
		document.forms[0].action="/student/sGet";
		document.forms[0].submit();
	}
	
	function sGetAll() {
		//alert("I am in sGetAll function");
		document.forms[0].action="/student/sGetAll";
		document.forms[0].submit();
	}
</script>

</head>
<body>
<form>
		<fieldset>
		<legend>Student Details</legend>
			<!-- Student ID:      --><input type="hidden" name="id" value="${studentObj.id}"><br>
			Student Name:   <input type="text" name="name" value="${studentObj.name}"><br>
			<%-- Passport ID:  <input type="number" name="passport.id" value="${studentObj.passport.id}"><br> --%> 
			Passport No:  <input type="text" name="passport.passportNo" value="${studentObj.passport.passportNo}"><br>
			
			 
			<c:if test="${studentObj.id==null}">
			
				<input type="button" value="SAVE"  onclick="sSave()">
				<input type="button" value="UPDATE" disabled="disabled" onclick="sUpdate()">
				
			</c:if>
			
			<c:if test="${studentObj.id!=null}">
			
				<input type="button" value="SAVE" disabled="disabled"  onclick="sSave()">
				<input type="button" value="UPDATE" onclick="sUpdate()">
				
			</c:if>
			
			<input type="button" value="GETALL" onclick="sGetAll()">
		
		</fieldset>
	</form>

<%
		List<Student> stuList = (List<Student>) request.getAttribute("listObj");
		if (stuList.size() > 0) {
	%>
	<table>
		<tr>
			<th>ID</th>
			<th>NAME</th>
			<th>AGE</th>
			<th>QUAL</th>
			<th>MARKS</th>
			<th>PASSPORT #</th>
			<th>PASSPORT ISSUEDATE</th>
			<th>PASSPORT EXPDATE</th>
		</tr>
		
		
		<c:forEach var="s" items="${listObj}" >
	
		<tr>
			<td><font color="red"><a href = "sGet?id=${s.id}">${s.id}</a></font></td>
			<td><font color="red">${s.name}</font></td>
			<td><font color="red">${s.age}</font></td>
			<td><font color="red">${s.qual}</font></td>
			<td><font color="red">${s.marks}</font></td>
			<td><font color="red">${s.passport.passportNo}</font></td>
			<td><font color="red">${s.passport.issueDate}</font></td>
			<td><font color="red">${s.passport.expDate}</font></td>
		</tr>
		</c:forEach>

	</table>

	<%
		} else {
	%>
	<h3>No Records found</h3>
	<%
		}
	%>
</body>
</html>