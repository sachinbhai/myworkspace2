package com.sachin.OneDirectionalRelationship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OneDirectionalRelationshipApplication {

	public static void main(String[] args) {
		SpringApplication.run(OneDirectionalRelationshipApplication.class, args);
	}

}
