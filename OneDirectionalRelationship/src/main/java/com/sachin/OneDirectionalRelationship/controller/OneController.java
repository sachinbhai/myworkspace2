package com.sachin.OneDirectionalRelationship.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sachin.OneDirectionalRelationship.entity.Passport;
import com.sachin.OneDirectionalRelationship.entity.Student;
import com.sachin.OneDirectionalRelationship.repos.PassportRepo;
import com.sachin.OneDirectionalRelationship.repos.StudentRepo;

@Controller
@RequestMapping("student")
public class OneController {
	
	@Autowired
	public StudentRepo stuRepo;
	
	@Autowired
	public PassportRepo passRepo;
	
	@RequestMapping("/home")
	public String myHome() {
		return "myHome";
	}
	
	
	@RequestMapping("/sSave")
	public String saveStudent(Student student) {
		student.getPassport().setStudent(student);
		stuRepo.save(student);
		return "myHome";
	}
	
	
	@RequestMapping("/sUpdate")
	public String updateStudent(Student student) {
		Passport p = student.getPassport();
		p.setStudent(student);
		//student.setPassport(p);
		stuRepo.save(student);
		return "myHome";
	}
	
	@RequestMapping("/sGet")
	public ModelAndView getStudent(int id) {
		Optional<Student> stu =  stuRepo.findById(id);
		Student student = stu.get();
		student.getPassport().setStudent(student);
		ModelAndView mv = new ModelAndView("getPopulated", "studentObj", student);
		return mv;
	}
	
	@RequestMapping("sGetAll")
	public ModelAndView getAllStudents() {
		List<Student> stuList =  stuRepo.findAll();
		ModelAndView mv = new ModelAndView("getAll", "listObj", stuList);
		return mv;
	}
}
