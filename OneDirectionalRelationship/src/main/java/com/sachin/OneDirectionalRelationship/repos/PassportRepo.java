package com.sachin.OneDirectionalRelationship.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sachin.OneDirectionalRelationship.entity.Passport;

@Repository
public interface PassportRepo extends JpaRepository<Passport, Integer>{

}
