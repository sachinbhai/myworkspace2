package com.sachin.OneDirectionalRelationship.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sachin.OneDirectionalRelationship.entity.Student;

@Repository
public interface StudentRepo extends JpaRepository<Student, Integer> {

}
