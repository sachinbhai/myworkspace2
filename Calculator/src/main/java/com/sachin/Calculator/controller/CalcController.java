package com.sachin.Calculator.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CalcController {
	@RequestMapping("home")
	public String calcMain() {
		return "calc";
	}
	
	@RequestMapping("add")
	public ModelAndView addValues(int val1, int val2) {
		int addResult = val1+val2;
		ModelAndView mv = new ModelAndView("resultView", "addResultAtt", addResult);
		return mv;
	}
	
	@RequestMapping("sub")
	public ModelAndView subValues(int val1, int val2) {
		int subResult = val1-val2;
		ModelAndView mv = new ModelAndView("resultSubView", "subResultAtt", subResult);
		return mv;
	}
	
	@RequestMapping("mul")
	public ModelAndView mulValues(int val1, int val2) {
		double mulResult = val1*val2;
		ModelAndView mv = new ModelAndView("resultMulView", "mulResultAtt", mulResult);
		return mv;
	}
	
	@RequestMapping("div")
	public ModelAndView divValues(int val1, int val2) {
		float divResult = val1/val2;
		ModelAndView mv = new ModelAndView("resultDivView", "divResultAtt", divResult);
		return mv;
	}
}
