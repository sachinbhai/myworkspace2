package com.slokam;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FirstServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		/*StudentDao dao = new StudentDao();
		List<Student> stuList = dao.getAllStudents();
		req.setAttribute("allStudents", stuList);
		*/
		PrintWriter pw = resp.getWriter();
		pw.write("<form action='studentsTable'>");
		pw.write("<input type='submit' value='GET ALL STUDENT DETAILS'>");
		pw.write("</form>");
	}
}
