package com.slokam;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SaveStudentServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("we are @ home");
		int id = Integer.parseInt(req.getParameter("id"));
		String name = req.getParameter("name");
		int age = Integer.parseInt(req.getParameter("age"));
		String qual = req.getParameter("qual");
		int marks = Integer.parseInt(req.getParameter("marks"));
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hibernate", "root", "root");
			
			
			if(id != 0) {
				PreparedStatement ps = con.prepareStatement("insert into student1 (id, name, age, qual, marks) values(?,?,?,?,?)");
				ps.setInt(1, id);
				ps.setString(2, name);
				ps.setInt(3, age);
				ps.setString(4, qual);
				ps.setInt(5, marks);
				ps.executeUpdate();
				ps.close();
			}else {
				PreparedStatement ps = con.prepareStatement("insert into student1 (name, age, qual, marks) values(?,?,?,?)");
				ps.setString(1, name);
				ps.setInt(2, age);
				ps.setString(3, qual);
				ps.setInt(4, marks);
				ps.executeUpdate();
				ps.close();
			}
			
			con.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
