package com.slokam.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.slokam.entity.Product;

public class ProductDao {
	
	public List<Product> getProductsByName(String name){
		List<Product> productList = new ArrayList<>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/webapp", "root", "root");
			PreparedStatement ps = con.prepareStatement("select * from product where name like ?");
			ps.setString(1, name+"%");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Product product = new Product();
				product.setId(rs.getInt("id"));
				product.setName(rs.getString("name"));
				product.setCat(rs.getString("cat"));
				product.setPrice(rs.getInt("price"));
				product.setAvail(rs.getInt("avail"));
				product.setColor(rs.getString("color"));
				
				productList.add(product);
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return productList;
	}

}
