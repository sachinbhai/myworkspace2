package com.slokam.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.slokam.entity.Student;

public class StudentDao {
	
	public void saveStudent(Student student) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hibernate", "root", "root");
			PreparedStatement ps = con.prepareStatement("insert into student1 (name, age, qual, marks) values(?,?,?,?)");
			ps.setString(1, student.getName());
			ps.setInt(2, student.getAge());
			ps.setString(3, student.getQual());
			ps.setInt(4, student.getMarks());
			ps.executeUpdate();
			
			ps.close();
			con.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public List<Student> getAllStudents() {
		List<Student> stuList = new ArrayList<Student>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hibernate", "root", "root");
			PreparedStatement ps = con.prepareStatement("select * from student1");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Student stu = new Student();
				stu.setId(rs.getInt("id"));
				stu.setName(rs.getString("name"));
				stu.setAge(rs.getInt("age"));
				stu.setQual(rs.getString("qual"));
				stu.setMarks(rs.getInt("marks"));
				stuList.add(stu);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return stuList;
	}
	
	public List<String> getQualifications() {
		List<String> qualList = new ArrayList<>();
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/webapp", "root", "root");
			PreparedStatement ps = con.prepareStatement("select name from qualifications");
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				qualList.add(rs.getString("name"));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return qualList;
	}
}
