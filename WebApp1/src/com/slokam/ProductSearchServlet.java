package com.slokam;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slokam.dao.ProductDao;
import com.slokam.entity.Product;

public class ProductSearchServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String name = req.getParameter("productName");
		ProductDao dao = new ProductDao();
		List<Product> productList = dao.getProductsByName(name);
		req.setAttribute("productName", name);
		req.setAttribute("products", productList);
		req.setAttribute("noRecords", productList.size());
		RequestDispatcher rd = req.getRequestDispatcher("/searchResult.jsp");
		rd.forward(req, resp);
		
	}
	
	/*@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String name = req.getParameter("productName");
		ProductDao dao = new ProductDao();
		List<Product> productList = dao.getProductsByName(name);
		req.setAttribute("products", productList);
		req.setAttribute("noRecords", name);
		RequestDispatcher rd = req.getRequestDispatcher("/searchResult.jsp");
		rd.forward(req, resp);
	}*/
}
