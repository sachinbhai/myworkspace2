package com.slokam;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slokam.dao.StudentDao;
import com.slokam.entity.Student;

public class StudentDisplayServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StudentDao dao = new StudentDao();
		List<Student> stuList = dao.getAllStudents();
		req.setAttribute("allStudents", stuList);
		RequestDispatcher rd = req.getRequestDispatcher("/displayRecords.jsp");
		rd.forward(req, resp);
	}
}
