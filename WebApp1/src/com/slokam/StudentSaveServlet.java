package com.slokam;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slokam.dao.StudentDao;
import com.slokam.entity.Student;

import jdk.nashorn.internal.ir.RuntimeNode.Request;

public class StudentSaveServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		Student stu = new Student();
		stu.setName(req.getParameter("name"));
		stu.setAge(Integer.parseInt(req.getParameter("age")));
		stu.setQual(req.getParameter("qual"));
		stu.setMarks(Integer.parseInt(req.getParameter("marks")));
		StudentDao dao = new StudentDao();
		dao.saveStudent(stu);
		
		List<Student> stuList = dao.getAllStudents();
		req.setAttribute("allStudents", stuList);
		RequestDispatcher rd = req.getRequestDispatcher("/displayRecords.jsp");
		rd.forward(req, resp);
	}
}
