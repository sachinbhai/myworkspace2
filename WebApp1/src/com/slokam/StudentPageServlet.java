package com.slokam;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slokam.dao.StudentDao;

public class StudentPageServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StudentDao dao = new StudentDao();
		List<String> qualList = dao.getQualifications();
		req.setAttribute("qualifications", qualList);
		req.getRequestDispatcher("studentPage.jsp").forward(req, resp);
	}
}
