<%@page import="com.slokam.entity.Product"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		List<Product> productList = (List<Product>) request.getAttribute("products");
	%>
	<%
		if ((Integer) request.getAttribute("noRecords") != 0) {
	%>
	<table>
		<tr>
			<th>NAME</th>
			<th>CATEGORY</th>
			<th>PRICE</th>
			<th>AVAILABILITY</th>
			<th>COLOR</th>
		</tr>
		<%
			for (Product p : productList) {
		%>
		<tr>

			<td><%=p.getName()%></td>
			<td><%=p.getCat()%></td>
			<td><%=p.getPrice()%></td>
			<td><%=p.getAvail()%></td>
			<td><%=p.getColor()%></td>

		</tr>
		<%
			}
		%>

	</table>
	<%
		} else {
	%>

	<h3>
		No records found for productName staring with ----
		<%=request.getParameter("productName")%>
	</h3>
	<%
		}
	%>

	<!-- JSTL below  -->

	<c:if test="${noRecords>0}">

		<table>
			<tr>
				<th>NAME</th>
				<th>CATEGORY</th>
				<th>PRICE</th>
				<th>AVAILABILITY</th>
				<th>COLOR</th>
			</tr>
			
			<c:forEach var="p" items="${products}" varStatus="status">
				<c:choose>
					<c:when test="${status.index%3==0}">
						<tr>
							<td><font color="red">${p.name}</font></td>
							<td><font color="red">${p.cat}</font></td>
							<td><font color="red">${p.price}</font></td>
							<td><font color="red">${p.avail}</font></td>
							<td><font color="red">${p.color}</font></td>
						</tr>
					</c:when>
					<c:when test="${status.index%3==1}">
						<tr>
							<td><font color="green">${p.name}</font></td>
							<td><font color="green">${p.cat}</font></td>
							<td><font color="green">${p.price}</font></td>
							<td><font color="green">${p.avail}</font></td>
							<td><font color="green">${p.color}</font></td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<td><font color="blue">${p.name}</font></td>
							<td><font color="blue">${p.cat}</font></td>
							<td><font color="blue">${p.price}</font></td>
							<td><font color="blue">${p.avail}</font></td>
							<td><font color="blue">${p.color}</font></td>
						</tr>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</table>
	</c:if>

	<c:if test="${noRecords==0}">
	No records found for productName staring with ----${productName}<br>
	</c:if>
	<a href="initialPage.html"><button>HOME</button></a>
</body>
</html>