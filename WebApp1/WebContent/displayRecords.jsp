<%@page import="com.slokam.entity.Student"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		List<Student> stuList = (List<Student>) request.getAttribute("allStudents");
	%>
	<table>
		<tr>
			<th>ID</th>
			<th>NAME</th>
			<th>AGE</th>
			<th>QUALIFICATION</th>
			<th>MARKS</th>
		</tr>
		<%
			for (Student s : stuList) {
		%>
		<tr>
			<td><%=s.getId()%></td>
			<td><%=s.getName()%></td>
			<td><%=s.getAge()%></td>
			<td><%=s.getQual()%></td>
			<td><%=s.getMarks()%></td>
		</tr>
		
		<%
			}
		%>
	</table>
	<a href="enterStudentDetails.jsp"><button>ADD STUDENT</button></a>


</body>
</html>