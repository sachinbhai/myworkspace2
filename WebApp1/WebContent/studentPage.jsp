<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
	function saveStudent() {
		alert=("Enter ID for Student");
		document.forms.my.action="saveStudent";
		document.forms.my.submit();
	}
	
	function updateStudent() {
		alert=("Enter ID for Student");
		document.forms.my.action="updateStudent";
		document.forms.my.submit();
	}
	
	function deleteStudent() {
		alert=("Enter ID for Student");
		document.forms.my.action="deleteStudent";
		document.forms.my.submit();
	}
</script>
</head>
<body>
	<form name="my">
		<fieldset>
			<legend>Enter Student Details</legend>
			Id:<input type="text" name="id"> (Enter ID for Student record's update/delete)<br>
			Name: <input type="text" name="name"><br> 
			Age:<input type="text" name="age"><br> 
			Qualification:
			<c:if test="${qualifications != null}">
				<select name="qual">
					<c:forEach var="q" items="${qualifications}">
						<option>${q}</option>
					</c:forEach>
				</select>
			</c:if>
			Marks:<input type="text" name="marks"><br>
		<input type="button" value="SAVE" onclick="saveStudent()"> 
		<input type="button" value="UPDATE" onclick="updateStudent()"> 
		<input type="button" value="DELETE" onclick="deleteStudent()">
		</fieldset>
	</form>

</body>
</html>