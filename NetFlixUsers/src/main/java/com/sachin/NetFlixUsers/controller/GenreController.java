package com.sachin.NetFlixUsers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.NetFlixUsers.entity.Genre;
import com.sachin.NetFlixUsers.entity.User;
import com.sachin.NetFlixUsers.repos.GenreRepo;
import com.sachin.NetFlixUsers.repos.UserRepo;

@RestController
@RequestMapping("genre")
public class GenreController {
	
	@Autowired
	private GenreRepo gRepo;
	
	@PostMapping("/saveGenre")
	public ResponseEntity saveGenre(@RequestBody Genre genre){
		gRepo.save(genre);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
}
