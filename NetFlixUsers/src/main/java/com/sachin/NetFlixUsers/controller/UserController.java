 package com.sachin.NetFlixUsers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.NetFlixUsers.entity.Country;
import com.sachin.NetFlixUsers.entity.User;
import com.sachin.NetFlixUsers.repos.CountryRepo;
import com.sachin.NetFlixUsers.repos.UserRepo;

@RestController
@RequestMapping("user")
public class UserController {
	
	@Autowired
	private UserRepo uRepo;
	
	@PostMapping("/saveUser")
	public ResponseEntity<User> saveUser(@RequestBody User user){
		uRepo.save(user);
		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}
}
