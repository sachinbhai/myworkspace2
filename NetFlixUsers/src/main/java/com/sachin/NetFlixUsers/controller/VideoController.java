package com.sachin.NetFlixUsers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.NetFlixUsers.entity.Video;
import com.sachin.NetFlixUsers.repos.VideoRepo;

@RestController
@RequestMapping("video")
public class VideoController {
	
	@Autowired
	private VideoRepo videoRepo;
	
	@PostMapping("/saveVideo")
	public ResponseEntity<Video> saveVideo(@RequestBody Video video){
		videoRepo.save(video);
		return new ResponseEntity<>(video, HttpStatus.CREATED);
	}
}
