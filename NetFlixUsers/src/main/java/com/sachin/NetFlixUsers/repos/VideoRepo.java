package com.sachin.NetFlixUsers.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sachin.NetFlixUsers.entity.Video;

@Repository
public interface VideoRepo extends JpaRepository<Video, Integer> {
	
	/*@Query(value="select v.id from user u " + 
			"join user_genre ug on u.id=ug.uid " + 
			"join video v on v.gnid=ug.gnid " + 
			"where u.id=2 and v.id not in (select v.id from video v " + 
			"join user_view_history vh on vh.vid=v.id " + 
			"where vh.uid=2)", nativeQuery=true)
	public List<Integer> getVideosByGenreByUserId(Integer id);*/
	
	@Query("select v from Video v, User u "
			+ "join u.countryList uc "
			+ "join v.videoCountry vc "
			+ "where vc.id=uc.id and u.id=?1")
	public List<Video> getVideosByCountryByUserId(Integer id);
	
	@Query("select v from Video v, User u "
			+ "join u.genreList ug "
			+ "join v.videoGenre vg "
			+ "where vg.id=ug.id and u.id=?1")
	public List<Video> getVideosByGenreByUserId(Integer id);
	
	@Query("select v from Video v join UserViewHistory vh where vh.videoView.id=v.id")
	public List<Video> getVideosWatchedByUser(Integer id);
	
}
