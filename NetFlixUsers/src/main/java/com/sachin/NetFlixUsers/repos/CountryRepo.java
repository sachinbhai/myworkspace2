package com.sachin.NetFlixUsers.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sachin.NetFlixUsers.entity.Country;

@Repository
public interface CountryRepo extends JpaRepository<Country, Integer> {

}
