package com.sachin.NetFlixUsers.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table
public class UserViewHistory {
	
	@Id
	@GeneratedValue
	private Integer id;
	@DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	private Date whenWatched;
	@ManyToOne()
	@JoinColumn(name="vid")
	private Video videoView;
	@ManyToOne()
	@JoinColumn(name="uid")
	private User user;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getWhenWatched() {
		return whenWatched;
	}
	public void setWhenWatched(Date whenWatched) {
		this.whenWatched = whenWatched;
	}
	public Video getVideoView() {
		return videoView;
	}
	public void setVideoView(Video videoView) {
		this.videoView = videoView;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

}
