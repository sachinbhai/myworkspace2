package com.sachin.NetFlixUsers.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table
@SequenceGenerator(name="user_sequence", allocationSize=1, initialValue=1)
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_sequence")
	private Integer id;
	private String name;
	
	@ManyToMany()
	@JoinTable(
			name="user_country",
			joinColumns= {@JoinColumn(name="uid")},
			inverseJoinColumns= {@JoinColumn(name="cid")}
			)
	private List<Country> countryList;
	
	@ManyToMany()
	@JoinTable(
			name="user_genre",
			joinColumns= {@JoinColumn(name="uid")},
			inverseJoinColumns= {@JoinColumn(name="gnid")}
			)
	private List<Genre> genreList;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Country> getCountryList() {
		return countryList;
	}
	public void setCountryList(List<Country> countryList) {
		this.countryList = countryList;
	}
	public List<Genre> getGenreList() {
		return genreList;
	}
	public void setGenreList(List<Genre> genreList) {
		this.genreList = genreList;
	}
}
