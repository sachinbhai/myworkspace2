package com.sachin.NetFlixUsers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NetFlixUsersApplication {

	public static void main(String[] args) {
		SpringApplication.run(NetFlixUsersApplication.class, args);
	}

}
