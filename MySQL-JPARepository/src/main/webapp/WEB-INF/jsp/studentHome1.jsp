<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page with Student OBJ populated</title>

<script type="text/javascript">

	function sSave() {
		var name = document.forms[0].name.value;
		var age = document.forms[0].age.value;
		var qual = document.forms[0].qual.value;
		var marks = document.forms[0].marks.value;
		
		if(name==""|age==""|qual==""|marks==""){
			alert("Please Enter Data");
			return;
		}
		document.forms[0].action="sSave";
		document.forms[0].submit();
	}
	function sDelete() {
		document.forms[0].action = "sDelete";
		document.forms[0].submit();
	}
	function sUpdate() {
		document.forms[0].action = "sUpdate";
		document.forms[0].submit();
	}
	function sGet() {
		document.forms[0].action = "sGet";
		document.forms[0].submit();
	}
	function sGetAll() {
		document.forms[0].action="sGetAll";
		document.forms[0].submit();
	}
</script>
</head>

<body>
	<form>
		<fieldset>
		<legend>Student Details</legend>
			<!-- Student ID: --><input type="hidden" name="id" value="${studentObj.id}"><br> 
			Student Name: <input type="text" name="name" value="${studentObj.name}"><br> 
			Student Age: <input type="number" name="age" value="${studentObj.age}"><br> 
			Student Qual: <input type="text" name="qual" value="${studentObj.qual}"><br> 
			Student Marks: <input type="number" name="marks" value="${studentObj.marks}"><br> 
			Passport ID: <input type="text" name="pptObj.passportNo" value="${studentObj.pptObj.id}"><br>
			Passport No:  <input type="text" name="pptObj.passportNo" value="${studentObj.pptObj.passportNo}"><br>
			Passport IssueDate:  <input type="date" name="pptObj.issueDate" value="${studentObj.pptObj.issueDate}"><br>
			Passport ExpDate:  <input type="date" name="pptObj.expDate" value="${studentObj.pptObj.expDate}"><br><br> 
			<%-- <!-- Passport FKey: --><input type="number" name="id" value="${studentObj.pptObj.student.id}"><br>	 --%>
			<c:if test="${studentObj.id==null }">
			
				<input type="button" value="SAVE"  onclick="sSave()">
				<input type="button" value="DEL" disabled="disabled" onclick="sDelete()">
				<input type="button" value="UPDATE" disabled="disabled" onclick="sUpdate()">
				
			</c:if>
			
			<c:if test="${studentObj.id!=null }">
			
				<input type="button" value="SAVE" disabled="disabled"  onclick="sSave()">
				<input type="button" value="DEL" onclick="sDelete()">
				<input type="button" value="UPDATE" onclick="sUpdate()">
				
			</c:if>
			
			<input type="button" value="GETALL" onclick="sGetAll()">
			
		</fieldset>
	</form>
	
	<%-- 
	<%
		if (request.getAttribute("studentObj") != null) {
	%>
	<table >
		<tr>
			<th>ID</th>
			<th>NAME</th>
			<th>AGE</th>
			<th>QUALIFICATION</th>
			<th>MARKS</th>
		</tr>

		<tr>
			<td><font color="red">${studentObj.id}</font></td>
			<td><font color="red">${studentObj.name}</font></td>
			<td><font color="red">${studentObj.age}</font></td>
			<td><font color="red">${studentObj.qual}</font></td>
			<td><font color="red">${studentObj.marks}</font></td>
		</tr>


	</table>

	<%
		} else {
	%>

	No Records found with this id #
	<%
		}
	%>

 --%>

</body>
</html>