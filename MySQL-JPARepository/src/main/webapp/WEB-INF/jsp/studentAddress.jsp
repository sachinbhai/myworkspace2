<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student Address Page</title>
<script type="text/javascript">
	function aSave() {
		document.forms[0].action="aSave";
		document.forms[0].submit();
	}
	function aDelete() {
		document.forms[0].action="aDelete";
		document.forms[0].submit();
	}
	function aUpdate() {
		document.forms[0].action="aUpdate";
		document.forms[0].submit();
	}
	function aDone() {
		document.forms[0].action="aDone";
		document.forms[0].submit();
	}
</script>
</head>
<body>
	<form>
		<fieldset>
			<legend>Address Details</legend>
			<!-- Address ID:      -->
			<input type="hidden" name="id"><br> 
			House Number: <input type="text" name="hNum"><br> 
			Street Name: <input type="text" name="stName"><br> 
			City: <input type="text" name="city"><br> 
			Pin Code: <input type="number" name="pin"><br> 
				
			<input type="button" value="SAVE" onclick="aSave()">
			<input type="button" value="DEL" onclick="aDelete()">
			<input type="button" value="UPDATE" onclick="aUpdate()">
			<input type="button" value="DONE" onclick="aDone()">

		</fieldset>
	</form>
</body>
</html>