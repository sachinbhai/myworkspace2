package com.sachin.MySQLJPARepository.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.sachin.MySQLJPARepository.entity.Address;
import com.sachin.MySQLJPARepository.entity.Passport;
import com.sachin.MySQLJPARepository.entity.Student;
import com.sachin.MySQLJPARepository.service.AddressDao;
import com.sachin.MySQLJPARepository.service.StudentDao;

@Controller
@SessionAttributes()
public class DemoController {
	
	@Autowired
	public StudentDao stuDao;
	
	@Autowired
	public AddressDao addDao;
	
	List<Address> addList = new ArrayList<>();
	
	@RequestMapping("sHome")
	public String studentHome() {
		return "studentHome";
	}
	
	@RequestMapping("sSave")
	public String saveStudent(Student student) {
		student.getPptObj().setStudent(student);
		
		
		/*for (int i=0;i<addList.size();i++) {
			student.getAddr().get(i).setStuObj(student);
		}*/
		stuDao.save(student);
		return "studentHome";
	}
	
	@RequestMapping("sDelete")
	public String deleteStudent(int id) {
		Optional<Student> stu =  stuDao.findById(id);
		Student student = stu.get();
		stuDao.deleteById(student.getId());
		return "studentHome";
	}
	
	@RequestMapping("sUpdate")
	public String updateStudent(Student student) {
		student.getPptObj().setStudent(student);
		stuDao.save(student);
		return "studentHome";
	}
	
	@RequestMapping("sGet")
	public ModelAndView getStudent(int id) {
		Optional<Student> stu =  stuDao.findById(id);
		Student student = stu.get();
		student.getPptObj().setStudent(student);
	
		ModelAndView mv = new ModelAndView("studentHome1", "studentObj", student);
		return mv;
	}
	
	@RequestMapping("sGetAll")
	public ModelAndView getAllStudents() {
		List<Student> stuList =  stuDao.findAll();
		ModelAndView mv = new ModelAndView("getAllStudentHome", "listObj", stuList);
		return mv;
	}
	
	@RequestMapping("sAddress")
	public String addAddress() {		
		return "studentAddress";
	}
	
	@RequestMapping("aSave")
	public ModelAndView saveAddress(Address address) {
		System.out.println(address.getStName());
		addList.add(address);
		System.out.println(addList.size());
		ModelAndView mv = new ModelAndView("studentAddress", "addListObj", addList);
		return mv;
	}
	
	@RequestMapping("aDelete")
	public String deleteAddress(int id) {
		Optional<Address> addr =  addDao.findById(id);
		Address address = addr.get();
		addDao.deleteById(address.getId());
		return "studentAddress";
	}
	
	@RequestMapping("aUpdate")
	public String updateAddress(Address address) {
		addDao.save(address);
		return "studentAddress";
	}
	
	@RequestMapping("aDone")
	public ModelAndView aDone() {
		//List<Address> addrList = addList;
		ModelAndView mv = new ModelAndView("studentHome","addListObj",addList);
		return mv;
	}
}
