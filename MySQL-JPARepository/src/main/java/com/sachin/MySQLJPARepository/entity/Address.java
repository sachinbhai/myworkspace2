package com.sachin.MySQLJPARepository.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Address {
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private Integer id;
		private String hNum;
		private String stName;
		private String city;
		private Integer pin;
		
		@ManyToOne(cascade=CascadeType.ALL)
		@JoinColumn(name="sid")
		private Student stuObj;
		
		public Student getStuObj() {
			return stuObj;
		}
		public void setStuObj(Student stuObj) {
			this.stuObj = stuObj;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String gethNum() {
			return hNum;
		}
		public void sethNum(String hNum) {
			this.hNum = hNum;
		}
		public String getStName() {
			return stName;
		}
		public void setStName(String stName) {
			this.stName = stName;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public Integer getPin() {
			return pin;
		}
		public void setPin(Integer pin) {
			this.pin = pin;
		}
		
		
}
