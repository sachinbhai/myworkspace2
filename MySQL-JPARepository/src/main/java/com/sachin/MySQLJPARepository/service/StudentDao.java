package com.sachin.MySQLJPARepository.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sachin.MySQLJPARepository.entity.Student;

@Repository
public interface StudentDao extends JpaRepository<Student, Integer> {

}
