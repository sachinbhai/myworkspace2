package com.sachin.MySQLJPARepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySqlJpaRepositoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(MySqlJpaRepositoryApplication.class, args);
	}

}
