package com.slokam.module1;
import java.util.Scanner;

public class TripInfo {	 
	
	public GetBudget getTripInfo() {
		Scanner scan = new Scanner(System.in);
		System.out.print("How many days are you going to spend travelling?");
		int numberOfDays1 = Integer.parseInt(scan.nextLine());
		System.out.print("how much money,in USD,are you planning to spend on your trip?" );
		int amount1 =Integer.parseInt(scan.nextLine());
		
		System.out.print("what is the three letter currency symbol for your travel destination ");
		String str1 = scan.nextLine();
		System.out.print("how many MXC are there in 1 USD?");
		double conversion1 =Double.parseDouble(scan.nextLine());
		
		GetBudget gb = new GetBudget();
		gb.setNumberOfDays(numberOfDays1);
		gb.setAmount(amount1);
		gb.setStr(str1);
		gb.setConversion(conversion1);
		
		return gb;
	}
	
}
