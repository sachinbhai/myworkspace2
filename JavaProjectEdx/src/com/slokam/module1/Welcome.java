package com.slokam.module1;

import java.util.Scanner;

public class Welcome {
	
	public static void main(String[] args) {
		System.out.println("Welcome to Vacation Planner!");
		System.out.print("What is your name?");
		Scanner scan = new Scanner(System.in);
		String str = scan.nextLine();
		System.out.print("Nice to meet you "+str+", where are you travelling to?");
		str = scan.nextLine();
		System.out.println("Great! "+ str +" sounds like a great trip.");
		System.out.println("**********");
		
		TripInfo ti = new TripInfo();
		
		GetBudget getBudget = ti.getTripInfo();
		
		System.out.println(getBudget.getNumberOfDays());
		
		
	}

}
