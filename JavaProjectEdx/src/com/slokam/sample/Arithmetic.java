package com.slokam.sample;

public class Arithmetic {
	public int x;
	public int y;
	
	public int multiply() {
		int z = this.x * this.y;
		return z;
	}
	
	public int add() {
		int z = this.x + this.y;
		return z;
	}
	
	public void setX(int x) {
		this.x=x*2;
	}
	
	public void setY(int y) {
		this.y=y*2;
	}
}
