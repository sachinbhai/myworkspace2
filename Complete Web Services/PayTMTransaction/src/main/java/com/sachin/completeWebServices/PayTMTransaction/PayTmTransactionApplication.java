package com.sachin.completeWebServices.PayTMTransaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class PayTmTransactionApplication {

	public static void main(String[] args) {
		SpringApplication.run(PayTmTransactionApplication.class, args);
	}

}
