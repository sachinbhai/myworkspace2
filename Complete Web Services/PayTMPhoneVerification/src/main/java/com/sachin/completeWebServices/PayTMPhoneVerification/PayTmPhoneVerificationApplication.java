package com.sachin.completeWebServices.PayTMPhoneVerification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class PayTmPhoneVerificationApplication {

	public static void main(String[] args) {
		SpringApplication.run(PayTmPhoneVerificationApplication.class, args);
	}

}
