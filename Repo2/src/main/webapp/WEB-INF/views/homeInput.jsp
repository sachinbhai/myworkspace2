<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">

	function sSave() {
		var name = document.forms[0].name.value;
		var age = document.forms[0].age.value;
		var qual = document.forms[0].qual.value;
		var marks = document.forms[0].marks.value;
		
		if(name==""|age==""|qual==""|marks==""){
			alert("Please Enter Data");
			return;
		} 
		document.forms[0].action="sSave";
		document.forms[0].submit();
	}
	function sDelete() {
		document.forms[0].action = "sDelete";
		document.forms[0].submit();
	}
	function sUpdate() {
		document.forms[0].action = "sUpdate";
		document.forms[0].submit();
	}
	function sGet() {
		document.forms[0].action = "sGet";
		document.forms[0].submit();
	}
	function sGetAll() {
		document.forms[0].action="sGetAll";
		document.forms[0].submit();
	}
</script>
</head>
<body>
<form>
		<fieldset>
		<legend>Student Details</legend>
			<!-- Student ID:      --><input type="hidden" name="id" value="${studentObj.id}"><br>
			Student Name:   <input type="text" name="name" value="${studentObj.name}"><br> 
			Student Age:    <input type="number" name="age" value="${studentObj.age}"><br> 
			Student Phone:   <input type="text" name="phone" value="${studentObj.phone}"><br>
			Passport No:  <input type="text" name="passport.passportNo" value="${studentObj.passport.passportNo}"><br>
			Passport IssueDate:  <input type="date" name="passport.issueDate" value="${studentObj.passport.issueDate}"><br>
			Passport ExpDate:  <input type="date" name="passport.expDate" value="${studentObj.passport.expDate}"><br><br>
			
			<c:if test="${studentObj.id==null }">
			
				<input type="button" value="SAVE"  onclick="sSave()">
				<input type="button" value="DEL" disabled="disabled" onclick="sDelete()">
				<input type="button" value="UPDATE" disabled="disabled" onclick="sUpdate()">
				
			</c:if>
			
			<c:if test="${studentObj.id!=null }">
			
				<input type="button" value="SAVE" disabled="disabled"  onclick="sSave()">
				<input type="button" value="DEL" onclick="sDelete()">
				<input type="button" value="UPDATE" onclick="sUpdate()">
				
			</c:if>
			
			<input type="button" value="GETALL" onclick="sGetAll()">
			
		</fieldset>
</form>

</body>
</html>