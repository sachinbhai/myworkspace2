package com.sachin.Repo2.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Range;
import org.springframework.web.multipart.MultipartFile;

@Entity
@Table
public class Product {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@NotBlank(message="Please Enter Valid Name")
	private String name;
	@NotNull(message="Please Enter a value btw 25k and 2000k")
	@Range(min=25000, max=2000000, message="Please Enter a value btw 25k and 2000k")
	private Integer price;
	@NotBlank(message="Please Enter a valid color")
	private String color;
	@NotNull(message="Please Enter a valid number")
	private Integer avail;
	@NotBlank
	@Pattern(regexp="^[A-Z]{5}[0-9]{4}$", message="Please enter valid number")
	private String panCard;
	
	@Transient
	private MultipartFile profile;
	
	
	public MultipartFile getProfile() {
		return profile;
	}
	public void setProfile(MultipartFile profile) {
		this.profile = profile;
	}
	
	public String getPanCard() {
		return panCard;
	}
	public void setPanCard(String panCard) {
		this.panCard = panCard;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Integer getAvail() {
		return avail;
	}
	public void setAvail(Integer avail) {
		this.avail = avail;
	}
		
}
