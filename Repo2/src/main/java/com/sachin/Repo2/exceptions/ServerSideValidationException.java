package com.sachin.Repo2.exceptions;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

public class ServerSideValidationException extends Exception {
	
	public ServerSideValidationException(){
		
	}
	
	public ServerSideValidationException(String message){
		super(message);
	}
		
}
