package com.sachin.Repo2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sachin.Repo2.entity.Product;
import com.sachin.Repo2.exceptions.ServerSideValidationException;
import com.sachin.Repo2.repo.ProductRepo;

@Service
public class ProductService {
	
	@Autowired
	public ProductRepo pRepo;
	
	
	public void saveProduct(Product product)  {
		pRepo.save(product);
	}
		
	public void moreThanAvail(Product product) throws ServerSideValidationException {
		if ((product.getAvail())>10) {
			
		}else {
			throw new ServerSideValidationException("Availability is low");
		}
	}
	
	public void imgValid(Product product) throws ServerSideValidationException {
		String fileName = product.getProfile().getOriginalFilename();
		if ((fileName.endsWith("jpg")) || (fileName.endsWith("jpeg"))) {
		}else {
			throw new ServerSideValidationException("File type is invalid");
		}
	}
	
}
