package com.slokam.oauth.allDaos;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.slokam.oauth.entities.OurUserDetails;
import com.slokam.oauth.entities.GrantedAuthorityImpl;


@Repository
public interface UserRepository  extends JpaRepository<OurUserDetails, Integer>{

	String string = "select a from OurUserDetails u join u.authorities a where u.username = ?1";
	 @Query(string)
	public Collection<GrantedAuthorityImpl> findByAuthorities(String username);
	
	public OurUserDetails findByUsername(String username);
	
}
