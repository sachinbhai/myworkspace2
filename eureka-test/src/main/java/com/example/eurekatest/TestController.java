package com.example.eurekatest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	private static final Logger log = LoggerFactory.getLogger(TestController.class);

	@RequestMapping("/test")
	public void loggingTest() {
		log.trace("log : trace");
		log.debug("log : debug");
		log.info("log : info");
		log.warn("log : warn");
		log.error("log : error");
	}

}
