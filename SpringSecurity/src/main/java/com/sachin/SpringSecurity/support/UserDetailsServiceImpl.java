package com.sachin.SpringSecurity.support;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.sachin.SpringSecurity.Service.DetailsService;
import com.sachin.SpringSecurity.repos.GrantedAuthorityRepo;
import com.sachin.SpringSecurity.repos.UserDetailsRepo;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
	
	
	@Autowired
	private DetailsService uService;
	/*@Autowired
	private GrantedAuthorityRepo gRepo;
*/	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		UserDetails userObj = uService.getUserByUsernameInService(username);
				
				/*new UserDetailsImpl();
		
		userDetailsImpl.setUsername(username);
		userDetailsImpl.setPassword("password");
		userDetailsImpl.setAccountNonExpired(true);
		userDetailsImpl.setAccountNonLocked(true);
		userDetailsImpl.setCredentialsNonExpired(true);
		userDetailsImpl.setEnabled(true);
		
		Optional<GrantedAuthorityImpl> g01 = gRepo.findById(1);
		GrantedAuthorityImpl g1 = g01.get();
		Optional<GrantedAuthorityImpl> g02 = gRepo.findById(2);
		GrantedAuthorityImpl g2 = g02.get();
		//g1.setAuthority("ROLE_user");
		//g2.setAuthority("ROLE_admin");
		
		Collection<GrantedAuthorityImpl> collectionX = new ArrayList<GrantedAuthorityImpl>();
		collectionX.add(g1);
		collectionX.add(g2);
		userDetailsImpl.setAuthorities(collectionX);
		
		uRepo.save(userDetailsImpl);
	*/	
				
		return userObj;
	}
	
}
