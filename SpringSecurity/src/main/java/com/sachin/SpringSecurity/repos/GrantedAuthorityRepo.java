package com.sachin.SpringSecurity.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sachin.SpringSecurity.support.GrantedAuthorityImpl;

@Repository
public interface GrantedAuthorityRepo extends JpaRepository<GrantedAuthorityImpl, Integer> {

}
