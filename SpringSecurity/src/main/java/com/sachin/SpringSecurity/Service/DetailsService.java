package com.sachin.SpringSecurity.Service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.sachin.SpringSecurity.repos.UserDetailsRepo;
import com.sachin.SpringSecurity.support.GrantedAuthorityImpl;
import com.sachin.SpringSecurity.support.UserDetailsImpl;

@Service
public class DetailsService {
	
	@Autowired
	private UserDetailsRepo uRepo;
	
	public UserDetails getUserByUsernameInService(String username) {
		UserDetailsImpl userDetailsObj = uRepo.findByUsername(username);
		Collection<GrantedAuthorityImpl> authorities = uRepo.getByUsername(username);
		userDetailsObj.setAuthorities(authorities);
		return userDetailsObj;
	}
}
