package com.sachin.SpringSecurity.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@EnableWebSecurity
public class UserSecurityConfig extends WebSecurityConfigurerAdapter{
	
	/*@Override
	@Bean
	protected UserDetailsService userDetailsService() {
		
		User.UserBuilder userBuilder = User.withDefaultPasswordEncoder();
		
		InMemoryUserDetailsManager userManager = new  InMemoryUserDetailsManager();
		
		UserDetails a = userBuilder.username("A").password("A1").roles("ADMIN", "USER").build();
		UserDetails b = userBuilder.username("B").password("B1").roles("USER").build();
		UserDetails c = userBuilder.username("C").password("C1").roles("USER").build();
		UserDetails d = userBuilder.username("D").password("D1").roles("USER").build();
		
		userManager.createUser(a);
		userManager.createUser(b);
		userManager.createUser(c);
		userManager.createUser(d);
		
		return userManager;
	}*/
	 
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http
		.csrf().disable()
		
		.authorizeRequests()
		.antMatchers("/security/loginPage").permitAll()
		.antMatchers("/security/CM3").hasAnyRole("user", "admin")
		.antMatchers("/security/CM4").hasRole("user")
		.antMatchers("/security/CM1").hasRole("admin")
		.antMatchers("/security/home", "/security/CM2").authenticated()
		
		.and()
		
		.formLogin()
		.loginPage("/security/loginPage")
		.usernameParameter("username")
		.passwordParameter("password")
		.loginProcessingUrl("/loginProcessingUrl")
		.defaultSuccessUrl("/security/home")
		.failureUrl("/security/loginPage");
		
	}
	
	/*@Bean
    public PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                return rawPassword.toString();
            }
            
            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return rawPassword.toString().equals(encodedPassword);
            }
        };
    }*/
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		
		return new BCryptPasswordEncoder();
	}
	
}
