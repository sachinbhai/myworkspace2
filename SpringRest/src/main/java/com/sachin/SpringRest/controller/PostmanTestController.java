package com.sachin.SpringRest.controller;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.SpringRest.entity.Student;
import com.sachin.SpringRest.repos.StudentRepo;


@RestController
@RequestMapping("student")
public class PostmanTestController {
	
	@Autowired
	public StudentRepo stuRepo;
	
	@GetMapping("/getOne/{id}")
	public Student getStudent(@PathVariable("id") /*@RequestParam(value="id", defaultValue="0")*/ Integer id) {
		Optional<Student> student = stuRepo.findById(id);
		Student stu = student.get();
		return stu;
	}
	
	@PostMapping("/saveOne")
	public Student saveStudent() {
		Student stu = new Student();
		stu.setId(1);
		stu.setName("sachin");
		stu.setPassport(null);
		stu.setAddr(null);
		stu.setCourses(null);
		return stu;
	}
	
	@GetMapping("/getAll")
	public List<Student> allStudents() {
		List<Student> stuList = stuRepo.findAll();
		return stuList;
	}
	
}
