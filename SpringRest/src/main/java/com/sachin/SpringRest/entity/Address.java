package com.sachin.SpringRest.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Address {
	@Id
	@GeneratedValue
	private Integer id;
	private String area;
	private Long pincode;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="sid")
	private Student stuAddr;
	
	public Student getStuAddr() {
		return stuAddr;
	}
	public void setStuAddr(Student stuAddr) {
		this.stuAddr = stuAddr;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public Long getPincode() {
		return pincode;
	}
	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}
	
	
}
