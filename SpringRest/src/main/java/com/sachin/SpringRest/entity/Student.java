package com.sachin.SpringRest.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ManyToAny;
import org.springframework.stereotype.Component;

@Entity
@Table
public class Student {
	@Id
	@GeneratedValue
	private Integer id;
	private String name;
	private Integer age;
	private Integer marks;
	private String qual;
	
	@OneToOne(mappedBy="student", cascade=CascadeType.ALL)
	private Passport passport;
	
	@OneToMany(mappedBy="stuAddr", cascade=CascadeType.ALL)
	private List<Address> addr;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(
		name ="student_course", 
		joinColumns = { @JoinColumn(name ="sid")},
		inverseJoinColumns = {@JoinColumn(name="cid")}
	)
	private List<Course> courses;
	
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Integer getMarks() {
		return marks;
	}
	public void setMarks(Integer marks) {
		this.marks = marks;
	}
	public String getQual() {
		return qual;
	}
	public void setQual(String qual) {
		this.qual = qual;
	}
	public List<Course> getCourses() {
		return courses;
	}
	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
	public List<Address> getAddr() {
		return addr;
	}
	public void setAddr(List<Address> addr) {
		this.addr = addr;
	}
	public Passport getPassport() {
		return passport;
	}
	public void setPassport(Passport passport) {
		this.passport = passport;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	

}
