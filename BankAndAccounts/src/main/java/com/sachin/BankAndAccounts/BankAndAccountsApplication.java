package com.sachin.BankAndAccounts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankAndAccountsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankAndAccountsApplication.class, args);
	}

}
