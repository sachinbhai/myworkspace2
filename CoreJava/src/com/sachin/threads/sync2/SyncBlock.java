package com.sachin.threads.sync2;

public class SyncBlock implements Runnable{
	@Override
	public void run() {
		m1();
		//m2();
		//m3();
	}
	
	public void m1() {
		synchronized (this) {
			for (int i=101; i<=200;i++ ) {
				System.out.println("Second :"+Thread.currentThread().getName()+"==="+i);
			}	
		}
		/*for (int i=1; i<=100;i++ ) {
			System.out.println("First :"+Thread.currentThread().getName()+"==="+i);
		}
		
		
		
		
		for (int i=201; i<=300;i++ ) {
			System.out.println("Third :"+Thread.currentThread().getName()+"==="+i);
		}*/

	}
	
	public void m2() {
		for (int i=1; i<=100;i++ ) {
			System.out.println("Fourth :"+Thread.currentThread().getName()+"==="+i);
		}
	}
	
	public void m3() {
		for (int i=1; i<=100;i++ ) {
			System.out.println("Fifth :"+Thread.currentThread().getName()+"==="+i);
		}
	}
	
	public static void main(String[] args) {
		SyncBlock sb = new SyncBlock();
		Thread t1 = new Thread(sb);
		t1.start();
		
		SyncBlock sb1 = new SyncBlock();
		Thread t2 = new Thread(sb1);
		t2.start();
	}
}
