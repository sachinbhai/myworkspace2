package com.sachin.threads.safety;

public class ThreadSafetyIssueDemo {
	
	public static void main(String[] args) {
		LoginPojo loginPojo = new LoginPojo();
		One one = new One();
		one.setLpj(loginPojo);
		Thread t1 = new Thread(one);
		t1.start();
		
		Two two = new Two();
		two.setLpj(loginPojo);
		Thread t2 = new Thread(two);
		t2.start();
	}
	
	public static void main1(String[] args) {
		
		One one = new One();
		Thread t1 = new Thread(one);
		t1.start();
		
		Two two = new Two();
		Thread t2 = new Thread(two);
		t2.start();
		
		
	}

}
