package com.sachin.threads.safety;

public class One implements Runnable {
	LoginPojo lpj;
	
	
	public void setLpj(LoginPojo lpj) {
		this.lpj = lpj;
	}


	@Override
	public void run() {
		lpj.setUserName("U1");
		lpj.setPassword("P1");
		
		try {
			Thread.sleep(3000);
			System.out.println(lpj.getUserName()+"::"+lpj.getPassword());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/*LoginPojo lpj = new LoginPojo();
	
	@Override
	public void run() {
		lpj.setUserName("U1");
		lpj.setPassword("P1");
		
		try {
			Thread.sleep(3000);
			System.out.println(lpj.getUserName()+"::"+lpj.getPassword());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
}
