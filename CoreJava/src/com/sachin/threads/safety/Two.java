package com.sachin.threads.safety;

public class Two implements Runnable{
	
	LoginPojo lpj;

	public void setLpj(LoginPojo lpj) {
		this.lpj = lpj;
	}


	@Override
	public void run() {
		lpj.setUserName("U2");
		lpj.setPassword("P2");
		
		try {
			Thread.sleep(3000);
			System.out.println(lpj.getUserName()+"::"+lpj.getPassword());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*LoginPojo lpj = new LoginPojo();
	
	@Override
	public void run() {
		lpj.setUserName("U2");
		lpj.setPassword("P2");
		
		try {
			Thread.sleep(3000);
			System.out.println(lpj.getUserName()+"::"+lpj.getPassword());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
}
