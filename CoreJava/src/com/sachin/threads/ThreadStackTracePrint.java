package com.sachin.threads;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ThreadStackTracePrint {
	public static void main(String[] args) {
		Thread t = new Thread();
		t.start();
		
		Map<Thread, StackTraceElement[]> threadStackNames = Thread.getAllStackTraces();
		Set<Entry<Thread, StackTraceElement[]>> s = threadStackNames.entrySet();
		for(Entry<Thread, StackTraceElement[]> e:s) {
			System.out.println("ThreadName:"+e.getKey());
			
			for(StackTraceElement ste: e.getValue()) {
				System.out.println("StackTraceElement:"+ste);
			}
			
			System.out.println("==================");
		}
	}
}
