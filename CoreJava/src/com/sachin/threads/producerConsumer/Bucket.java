package com.sachin.threads.producerConsumer;

public class Bucket {
	
	String water="";
	private boolean isAvail;
	
	public synchronized void setWater(int number) {
		if(isAvail) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.water= "Bucket ====="+number;
		System.out.println("set water==="+ this.water);
		this.isAvail=true;
		notifyAll();
	}
	
	public synchronized void getWater() {
		if(!isAvail) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("get water==="+ this.water);
		this.isAvail=false;
		notifyAll();
	}
}
