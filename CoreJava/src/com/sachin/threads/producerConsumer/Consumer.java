package com.sachin.threads.producerConsumer;

public class Consumer implements Runnable {
	private Bucket b;
	
	Consumer (Bucket b){
		this.b=b;
	}
	
	@Override
	public void run() {
		for(int i=1;i<=100;i++) {
			b.getWater();
		}
	}
}
