package com.sachin.threads.producerConsumer;

public class Producer implements Runnable {
	private Bucket b;
	
	Producer(Bucket b){
		this.b=b;
	}
	
	@Override
	public void run() {
		for(int i=1;i<=100;i++) {
			b.setWater(i);
		}
	}
}
