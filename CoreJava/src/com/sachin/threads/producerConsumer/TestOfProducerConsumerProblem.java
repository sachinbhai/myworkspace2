package com.sachin.threads.producerConsumer;

public class TestOfProducerConsumerProblem {
	
	public static void main(String[] args) {
		
		Bucket b = new Bucket();

		Consumer c = new Consumer(b);
		Thread t2 = new Thread(c);
		t2.start();
		
		Producer p = new Producer(b);
		Thread t1 = new Thread(p);
		t1.start();
		
		
	}

}
