package com.sachin.threads;

public class DeamonExample {
	public static void main(String[] args) {
		
		DeamonSupport ds = new DeamonSupport();
		Thread th = new Thread(ds);
		th.setDaemon(true);
		th.start();
		
		for(int i=1;i<=100; i++) {
			System.out.println("main method...."+i);
		}
		
		
	}
}
