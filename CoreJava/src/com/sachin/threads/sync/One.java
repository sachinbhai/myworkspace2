package com.sachin.threads.sync;

public class One implements Runnable{

	
	@Override
	public void run() {
		System.out.println("run  method:"+ Thread.currentThread().getName());
		System.out.println("=============");
		withdraw();
		System.out.println("=============");
		m1();
		System.out.println("=============");
		m2();
		System.out.println("=============");
	}
	
	public synchronized void withdraw() {
		for(int i=0; i<100; i++) {
			System.out.println(Thread.currentThread().getName()+":sync withdraw"+i);
		}
	}
	
	public void m1() {
		
		for(int i=0; i<100; i++) {
			System.out.println(Thread.currentThread().getName()+":normal m1:"+i);
		}
	}
	
	public synchronized void m2() {
		
		for(int i=0; i<100; i++) {
			System.out.println(Thread.currentThread().getName()+":sync m2:"+i);
		}
	}
}
