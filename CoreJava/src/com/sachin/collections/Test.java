package com.sachin.collections;

import java.util.ArrayList;
import java.util.List;

public class Test {
	public static void main(String[] args) {
		
		Student stu1 = new Student();
		Employee emp1 = new Employee();
		Object obj1 = new Object();
		/*Employee[] empArr = new Employee[3];
		Student[] stuArr = new Student[3];
		Object[] objArr = new Object[3];
		
		empArr[0] = stu1;
		stuArr[0] = stu1;
		objArr[0] = stu1;
		
		empArr[1] = emp1;
		//stuArr[1] = emp1;
		objArr[1] = emp1;
		
		empArr[2] = (Employee) obj1;
		stuArr[2] = (Student) obj1;
		objArr[2] = obj1;*/	
		
		List<Employee> empList = new ArrayList<>();
		List<Student> stuList = new ArrayList<>();
		List<Object> objList = new ArrayList<>();
		
		for(int i=0; i<5; i++) {
			Student stu = new Student();
			Employee emp = new Employee();
			Object obj = new Object();
			
			stuList.add(stu);
			empList.add(stu);
			empList.add(emp);
			objList.add(stu);
			objList.add(emp);
			objList.add(obj);
		}
		System.out.println("size of stuList: "+stuList.size());
		System.out.println("size of empList: "+empList.size());
		System.out.println("size of objList: "+objList.size());
		
	}
}
