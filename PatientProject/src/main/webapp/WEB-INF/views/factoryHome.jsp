<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<form action="/factory/patient">
	
		File Type(excell/xml/text)::<input type="text" name="type"><br>
		Patient Name::<input type="text" name="name"/><br>
		Patient Phone::<input type="text" name="phone"/><br>
		Patient Aadhar Card #::<input type="text" name="aadharCard"/><br>
		Patient dob::<input type="text" name="dob"/><br>
	
	<input type="submit" value="factoryTest">
	</form>
	
	</form><br>===================================<br>
	
	<form action="/factory/appointment">
	
		File Type(excell/xml/text)::<input type="text" name="type"><br>
		Appointment Date::<input type="text" name="date"/><br>
		Appointment Time::<input type="text" name="time"/><br>
	<input type="submit" value="factoryTest">
	</form>
	
	</form><br>===================================<br>
	
	<form action="/factory/doctor">
	
		File Type(excell/xml/text)::<input type="text" name="type"><br>
		Doctor Name::<input type="text" name="name"/><br>
		Doctor Specialization::<input type="text" name="specialization"/><br>
			
	<input type="submit" value="factoryTest">
	</form>

</body>
</html>