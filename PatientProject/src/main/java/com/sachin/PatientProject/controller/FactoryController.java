package com.sachin.PatientProject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sachin.PatientProject.entity.Appointment;
import com.sachin.PatientProject.entity.Doctor;
import com.sachin.PatientProject.entity.Patient;
import com.sachin.PatientProject.factoryUtil.AbstractFactory;
import com.sachin.PatientProject.factoryUtil.appointment.IAppointmentDao;
import com.sachin.PatientProject.factoryUtil.doctor.IDoctorDao;
import com.sachin.PatientProject.factoryUtil.patient.IPatientDao;

@Controller
@RequestMapping("factory")
public class FactoryController {
	
	@RequestMapping("/home")
	public ModelAndView fHome() {
		return new ModelAndView("factoryHome", "patientAtt", new Patient());
	}
	
	@RequestMapping("/patient")
	public ModelAndView saveP(String type, Patient patient) throws Exception {
		AbstractFactory af = AbstractFactory.getFactory(type);
		IPatientDao ip = af.getPatientDao();
		ip.savePatient(patient);
		return new ModelAndView("factoryHome", "patientAtt", new Patient());
	}
	
	@RequestMapping("/doctor")
	public ModelAndView saveD(String type, Doctor doctor) throws Exception {
		AbstractFactory af = AbstractFactory.getFactory(type);
		IDoctorDao id = af.getDoctorDao();
		id.saveDoctor(doctor);
		return new ModelAndView("factoryHome", "patientAtt", new Patient());
	}
	
	@RequestMapping("/appointment")
	public ModelAndView saveA(String type, Appointment appt) throws Exception {
		AbstractFactory af = AbstractFactory.getFactory(type);
		IAppointmentDao ia = af.getAppointmentDao();
		ia.saveAppointment(appt);
		return new ModelAndView("factoryHome", "patientAtt", new Patient());
	}
}
