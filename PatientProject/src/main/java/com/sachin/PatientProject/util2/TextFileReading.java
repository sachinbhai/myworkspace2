package com.sachin.PatientProject.util2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.sachin.PatientProject.entity.Patient;

@Component
public class TextFileReading implements IReadingStrategyInterface {

		@Override
		public List<Patient> getStartegy(File file) throws Exception {
			List<Patient> patientList = new ArrayList<>();
			
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			String str = br.readLine();
			
			while(str!=null) {
				String[] strArr = str.split(",");
				
				String name = strArr[0];
				Long phone = Long.parseLong(strArr[1]);
				String aadharCard = strArr[2];
				Date dob = new SimpleDateFormat("yyyy-MM-dd").parse(strArr[3]);
				
				Patient patient = new Patient();
				
				patient.setName(name);
				patient.setDob(dob);
				patient.setAadharCard(aadharCard);
				patient.setPhone(phone);
				
				patientList.add(patient);
				str=br.readLine();
			}
			
			br.close();
			fr.close();
			return patientList;
		}
}
