package com.sachin.PatientProject.factoryUtil.doctor;

import com.sachin.PatientProject.entity.Doctor;

public interface IDoctorDao {
	public void saveDoctor(Doctor doctor);
}
