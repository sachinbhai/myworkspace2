package com.sachin.PatientProject.factoryUtil.appointment;

import com.sachin.PatientProject.entity.Appointment;
import com.sachin.PatientProject.factoryUtil.ExcellFactory;

public class AppointmentExcellDao implements IAppointmentDao {
	@Override
	public void saveAppointment(Appointment appt) {
		System.out.println("save appointment object in excell");
	}
}
