package com.sachin.PatientProject.factoryUtil;

import com.sachin.PatientProject.factoryUtil.appointment.AppointmentXMLDao;
import com.sachin.PatientProject.factoryUtil.appointment.IAppointmentDao;
import com.sachin.PatientProject.factoryUtil.doctor.DoctorXMLDao;
import com.sachin.PatientProject.factoryUtil.doctor.IDoctorDao;
import com.sachin.PatientProject.factoryUtil.patient.IPatientDao;
import com.sachin.PatientProject.factoryUtil.patient.PatientXMLDao;

public class XMLFactory extends AbstractFactory {
	
	@Override
	public IPatientDao getPatientDao() {
		return new PatientXMLDao();
	}
	
	@Override
	public IDoctorDao getDoctorDao() {
		return new DoctorXMLDao();
	}
	
	@Override
	public IAppointmentDao getAppointmentDao() {
		return new AppointmentXMLDao();
	}
}
