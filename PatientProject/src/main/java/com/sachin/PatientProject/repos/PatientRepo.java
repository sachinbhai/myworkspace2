package com.sachin.PatientProject.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sachin.PatientProject.entity.Patient;

@Repository
public interface PatientRepo extends JpaRepository<Patient, Integer> {

}
