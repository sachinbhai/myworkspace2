package com.sachin.PatientProject.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RespMediator {
	@Autowired
	public PatientDataUpload patientDataUpload;
	@Autowired
	public ReadUpdatedFile readUpdatedFile;
	@Autowired
	public ValidateNameData validateNameData;
	@Autowired
	public ValidatePhoneData validatePhoneData;
	
	public IUploaderInterface getFirstResp() {
		patientDataUpload.setNextResp(readUpdatedFile);
		readUpdatedFile.setNextResp(validateNameData);
		validateNameData.setNextResp(validatePhoneData);
		return patientDataUpload;
	}
}
