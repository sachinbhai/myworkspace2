package com.sachin.PatientProject.util;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class PatientDataUpload extends IUploaderInterface {
	
	public static Logger log = LoggerFactory.getLogger(PatientDataUpload.class);
	@Value("${fileLocation}")
	public String folderName; 
	@Override
	public void performTask(RespPojo rPojo) throws Exception, IOException {
		File file = dataUpdate(rPojo.getMultipart());
		log.info("multipart file is saved in server");
		rPojo.setFile(file);
		gotoNextResp(rPojo);
	}
	
	private File dataUpdate(MultipartFile multipart) throws Exception {
		
		String fileName = multipart.getOriginalFilename();
		File file = new File(folderName+fileName);
		
		if(fileName.endsWith("xlsx")||fileName.endsWith("xls")) {
			multipart.transferTo(file);
			log.info("multipart-file is transferred");
		}else {
			throw new Exception();
		}
		return file;
	}
}
