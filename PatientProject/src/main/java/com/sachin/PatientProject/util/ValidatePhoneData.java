package com.sachin.PatientProject.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sachin.PatientProject.entity.Patient;
import com.sachin.PatientProject.repos.PatientRepo;

@Component
public class ValidatePhoneData extends IUploaderInterface{
	
	Logger log = LoggerFactory.getLogger(ValidatePhoneData.class);
	@Autowired
	public PatientRepo pRepo;
	@Override
	public void performTask(RespPojo rPojo) throws Exception, IOException {
		log.info("start of Phone validation");
		List<Patient> patientList = rPojo.getPatientList();
		Iterator<Patient> patients = patientList.iterator();
		while(patients.hasNext()) {
			Patient p = patients.next();
			if(p.getPhone()<1000000000 || p.getPhone()>9999999999L) {
				patients.remove();
			}
		}
		/*patients.forEachRemaining(p -> {
		if(p.getPhone()<1000000000 || p.getPhone()>9999999999L) {
			patients.remove();
		}
		} );*/
		log.info("end of Phone validation");
		rPojo.setPatientList(patientList);
		System.out.println(patientList.size()+"in validPhone");
	}
}
