package com.sachin.PatientProject.util;

import java.io.IOException;

public abstract class IUploaderInterface {
	
	public abstract void performTask(RespPojo rPojo) throws Exception, IOException;
	
	private IUploaderInterface nextResp;
	
	public void setNextResp(IUploaderInterface nextResp) {
		this.nextResp = nextResp;
	}
	
	public void gotoNextResp(RespPojo rPojo) throws Exception, IOException {
		if (nextResp != null) {
			nextResp.performTask(rPojo);
		}
	}

}
