package com.sachin.PatientProject.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sachin.PatientProject.entity.Patient;
import com.sachin.PatientProject.util2.IReadingStrategyInterface;
import com.sachin.PatientProject.util2.ReadingMediator;

@Component
public class ReadUpdatedFile extends IUploaderInterface{

	Logger log = LoggerFactory.getLogger(ReadUpdatedFile.class);
	
	List<Patient> patientList = new ArrayList<>();
	
	@Override
	public void performTask(RespPojo rPojo) throws Exception, IOException {
		log.info("start of reading data task");
		File file = rPojo.getFile();
		String filePath = file.getAbsolutePath();
		String fileType = filePath.substring(filePath.lastIndexOf('.'));
		
		IReadingStrategyInterface ir = ReadingMediator.chosenStrategy(fileType);
		
		List<Patient> patientList = ir.getStartegy(file);
		
		rPojo.setPatientList(patientList);
		System.out.println(patientList.size()+" in readUpdated");
		log.info("end of reading data task");
		gotoNextResp(rPojo);
		
	}
}
