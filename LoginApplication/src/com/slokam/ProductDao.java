package com.slokam;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import com.slokam.entity.Product;

public class ProductDao {
	
	public void saveProductDetails(Product product) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/webapp", "root", "root");
			PreparedStatement ps = con.prepareStatement("insert into product (name, cat, price, avail, color) value(?,?,?,?,?)");
			ps.setString(1, product.getName());
			ps.setString(2, product.getCat());
			ps.setInt(3, product.getPrice());
			ps.setInt(4, product.getAvail());
			ps.setString(5, product.getColor());
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
