package com.slokam;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slokam.entity.Product;

public class LoginCheckServlet extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String userName = req.getParameter("userName");
		String password = req.getParameter("password");
		UserDao uDao = new UserDao();
		boolean result = uDao.checkLogin(userName, password);
		resp.setContentType("text/html");
		PrintWriter pw = resp.getWriter();
		if (result) {
			//now, go to dBase to bring product details.
			//display data in html page
			List<Product> productList = uDao.getProductDetails();
			Integer numOfProducts = productList.size();
			req.setAttribute("productCount", numOfProducts);
			req.setAttribute("allProducts", productList);
			RequestDispatcher rd=req.getRequestDispatcher("/Home.jsp");
	        rd.forward(req, resp);	        
		}else {
			pw.write("LOGIN FAILURE");
		}
	}
}
