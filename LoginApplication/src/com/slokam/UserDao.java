package com.slokam;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.slokam.entity.Product;

public class UserDao {
	
	public boolean checkLogin(String userName, String password) {
		
		boolean result = false;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hibernate", "root", "root");
			PreparedStatement ps = con.prepareStatement("select * from user where userName=? and password=?");
			ps.setString(1, userName);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				result=true;
			}else {
				result=false;
			}
			
			rs.close();
			ps.close();
			con.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public List<Product> getProductDetails(){
		
		List<Product> productList = new ArrayList<Product>();
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/webapp", "root", "root");
			PreparedStatement ps = con.prepareStatement("select * from product");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Product product = new Product();
				product.setId(rs.getInt("id"));
				product.setName(rs.getString("name"));
				product.setCat(rs.getString("cat"));
				product.setPrice(rs.getInt("price"));
				product.setAvail(rs.getInt("avail"));
				product.setColor(rs.getString("color"));
				
				productList.add(product);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return productList;
	}

}
