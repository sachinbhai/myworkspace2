package com.slokam;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.DispatcherType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginPageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter pw = resp.getWriter();
		pw.write("<form action='loginCheck'>");
		pw.write("UserName: <input type='text' name='userName'> <br>");
		pw.write("Password: <input type='password' name='password'> <br>");
		pw.write("<input type='submit' value='LOGIN'>");
		pw.write("</form>");
		
		
	}
	
}
