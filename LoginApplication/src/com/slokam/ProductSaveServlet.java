package com.slokam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slokam.entity.Product;

public class ProductSaveServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Product product = new Product();
		product.setName(req.getParameter("name"));
		product.setCat(req.getParameter("cat"));
		product.setPrice(Integer.parseInt(req.getParameter("price")));
		product.setAvail(Integer.parseInt(req.getParameter("avail")));
		product.setColor(req.getParameter("color"));
		
		ProductDao pDao = new ProductDao();
		pDao.saveProductDetails(product);
		
		UserDao dao = new UserDao();
		List<Product> productList = dao.getProductDetails();
		
		req.setAttribute("allProducts", productList);
		RequestDispatcher rd = req.getRequestDispatcher("/Home.jsp");
		rd.forward(req, resp);
		
	}
}
