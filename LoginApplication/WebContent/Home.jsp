<%@page import="java.util.List"%>
<%@page import="com.slokam.UserDao"%>
<%@page import="com.slokam.entity.Product"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<%-- 	<h1> We are @ Home</h1>
	<%= request.getAttribute("productCount")  %>
	<% Integer productCount = (Integer)request.getAttribute("productCount");
	out.print(productCount);%> --%>
	
	<% List<Product> products = (List<Product>) request.getAttribute("allProducts"); %>
	<table>
		<tr> 	<th>ID</th> <th>NAME</th> <th>CAT</th>
				<th>PRICE</th> <th>AVAIL</th> <th>COLOR</th>
		</tr>
		<% for(Product p:products){ %>
		<tr>	<td><%=p.getId() %></td>
				<td><%=p.getName() %></td>
				<td><%=p.getCat() %></td>
				<td><%=p.getPrice() %></td>
				<td><%=p.getAvail() %></td>
				<td><%=p.getColor() %></td>
		</tr>
		<%}%>		
	</table>
	
	<a href="product.jsp"> <button>ADD PRODUCT</button> </a>
	
</body>
</html>