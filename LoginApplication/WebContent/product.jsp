<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="saveProduct">
		<fieldset>
			<legend> ADD PRODUCT DETAILS</legend>
			Name: 		<input type="text" name="name">
			Category: 	<input type="text" name="cat">
			Price:	 	<input type="text" name="price">
			Available:	<input type="text" name="avail">
			Color:		<input type="text" name="color">
			<input type="submit" value="SAVE">
		</fieldset>
	</form>
</body>
</html>