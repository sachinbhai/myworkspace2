<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
<script type="text/javascript">
	function sSave() {
		document.forms[0].action="sSave";
		document.forms[0].submit();
	}
	function sDelete() {
		document.forms[0].action="sDelete";
		document.forms[0].submit();
	}
</script>

</head>
<body>
	<form>
		<fieldset>
			Student ID:<input type="number" name="id"><br>
			Student Name:<input type="text" name="name"><br> 
			Student Age:<input type="number" name="age"><br> 
			Student Qual:<input type="text" name="qual"><br> 
			Student Marks:<input type="number" name="marks"><br> 
			<input type="button" value="SAVE" onclick="sSave()"> 
			<input type="button" value="DEL" onclick="sDelete()">
		</fieldset>

	</form>

</body>
</html>