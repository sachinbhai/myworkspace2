package com.slokam.oauth.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Entity
@Table(name = "userGrantedAuthority")
@Component
public class GrantedAuthorityImpl implements GrantedAuthority {
	@Id
	@GeneratedValue
	private Integer id;
	private String authority;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return this.authority;
	}

}
