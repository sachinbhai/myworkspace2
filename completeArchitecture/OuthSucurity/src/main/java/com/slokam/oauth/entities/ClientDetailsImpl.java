package com.slokam.oauth.entities;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.stereotype.Component;

@Component
public class ClientDetailsImpl implements ClientDetails{
	private String clientId;
	private Set<String> resourceIds;
	private boolean secretRequired;
	private String clientSecret;
	private boolean scoped;
	private Set<String> scope;
	private Set<String> authorizedGrantTypes;
	private Set<String> registeredRedirectUri;
	private Collection<GrantedAuthority> authorities;
	private Integer accessTokenValiditySeconds;
	private Integer refreshTokenValiditySeconds;
	private boolean autoApprove;
	private Map<String, Object> additionalInformation;
	
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	@Override
	public String getClientId() {
		return this.clientId;
	}
	
	public void setResourceIds(Set<String> resourceIds) {
		this.resourceIds = resourceIds;
	}
	@Override
	public Set<String> getResourceIds() {
		return this.resourceIds;
	}
	
	public void setSecretRequired(boolean secretRequired) {
		this.secretRequired = secretRequired;
	}
	@Override
	public boolean isSecretRequired() {
		return this.secretRequired;
	}
	
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	@Override
	public String getClientSecret() {
		return this.clientSecret;
	}
	
	public void setScoped(boolean scoped) {
		this.scoped = scoped;
	}
	@Override
	public boolean isScoped() {
		return this.scoped;
	}
	
	public void setScope(Set<String> scope) {
		this.scope = scope;
	}
	@Override
	public Set<String> getScope() {
		return this.scope;
	}
	
	public void setAuthorizedGrantTypes(Set<String> authorizedGrantTypes) {
		this.authorizedGrantTypes = authorizedGrantTypes;
	}
	@Override
	public Set<String> getAuthorizedGrantTypes() {
		return this.authorizedGrantTypes;
	}
	
	public void setRegisteredRedirectUri(Set<String> registeredRedirectUri) {
		this.registeredRedirectUri = registeredRedirectUri;
	}
	@Override
	public Set<String> getRegisteredRedirectUri() {
		return this.registeredRedirectUri;
	}
	
	public void setAuthorities(Collection<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}
	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	public void setAccessTokenValiditySeconds(Integer accessTokenValiditySeconds) {
		this.accessTokenValiditySeconds = accessTokenValiditySeconds;
	}
	@Override
	public Integer getAccessTokenValiditySeconds() {
		return this.accessTokenValiditySeconds;
	}

	public void setRefreshTokenValiditySeconds(Integer refreshTokenValiditySeconds) {
		this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
	}
	@Override
	public Integer getRefreshTokenValiditySeconds() {
		return this.refreshTokenValiditySeconds;
	}

	public void setAutoApprove(boolean autoApprove) {
		this.autoApprove = autoApprove;
	}
	@Override
	public boolean isAutoApprove(String scope) {
		return this.autoApprove;
	}

	public void setAdditionalInformation(Map<String, Object> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
	@Override
	public Map<String, Object> getAdditionalInformation() {
		return this.additionalInformation;
	}

}
