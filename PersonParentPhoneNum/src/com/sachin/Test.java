package com.sachin;

import java.util.Random;
import java.util.Scanner;

public class Test {
	public Long getPhoneNum(Person person) {
		Long phone = null;
		phone = person.getPhone();
		while(phone==null) {
		
			if(person.getParent()==null) {
				phone=person.getPhone();
				break;
			}
			
			if(phone==null) {
				phone = person.getParent().getPhone();
			}
			person = person.getParent();
		}
		return phone;
	}
	
	public static void main(String[] args) {
	
		Person a = new Person();
		a.setId(1);
		a.setAge(110);
		a.setName("Dashrath");
		a.setParent(null);
		a.setPhone(null);
		
		Person b = new Person();
		b.setId(2);
		b.setAge(85);
		b.setName("Ram");
		b.setParent(a);
		b.setPhone(45678L);
		
		Person c = new Person();
		c.setId(3);
		c.setAge(90);
		c.setName("Bharath");
		c.setParent(a);
		c.setPhone(null);
		
		Person h = new Person();
		h.setId(7);
		h.setAge(70);
		h.setName("krishna");
		h.setParent(c);
		h.setPhone(null);
		
		Person d = new Person();
		d.setId(4);
		d.setAge(60);
		d.setName("Lav");
		d.setParent(b);
		d.setPhone(23458L);
		
		Person e = new Person();
		e.setId(5);
		e.setAge(55);
		e.setName("Kush");
		e.setParent(b);
		e.setPhone(23459L);
		
		Person f = new Person();
		f.setId(6);
		f.setAge(57);
		f.setName("Dharma");
		f.setParent(b);
		f.setPhone(null);
		
		Test x = new Test();
		Long phone = x.getPhoneNum(f);
		System.out.println(phone);
		//System.out.println(f.getPhone());
	}

}
