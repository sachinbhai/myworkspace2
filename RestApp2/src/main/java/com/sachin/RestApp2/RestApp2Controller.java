package com.sachin.RestApp2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class RestApp2Controller {
	
	@Autowired
	private IRestApp1Feign restApp1Feign;
	
	@GetMapping("/restApp2Test")
	public String runApp2(){
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> re = restTemplate.getForEntity("http://localhost:2010/restApp1Test", String.class);
		String str = re.getBody();
		return "restApp2Test"+str;
	}
	
	@GetMapping("/restApp2Test2")
	public String runApp2Test2(){
		return "restApp2Test2::"+restApp1Feign.getTest();
		
	}
}
