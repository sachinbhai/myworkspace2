package com.sachin.RestApp2;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@FeignClient(name="RESTAPP1")
public interface IRestApp1Feign {
	
	@RequestMapping("/restApp1Test")
	public String getTest();
}
