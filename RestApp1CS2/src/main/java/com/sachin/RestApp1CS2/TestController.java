package com.sachin.RestApp1CS2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	@Value("${ClientOne.Test.Data:MyDefaultData}")
	private String data;
	
	@RequestMapping("/getData")
	private String getData() {
		return "Data from rest::"+data;
	}
}
